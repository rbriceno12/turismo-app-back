const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const db = require('../db/db');
require('dotenv').config(); // Load environment variables from .env file
const secret = process.env.SECRET_KEY;

// Function to generate a JWT token
const generateToken = (userId) => {
    const payload = {
      userId,
    };
  
    return jwt.sign(payload, secret, { expiresIn: '24h' }); // Set appropriate expiration time
};
  
  
// Middleware to verify JWT token (replace with protected routes)
const verifyToken = async (req, res, next) => {

    const authHeader = req.headers.authorization;
    if (!authHeader || !authHeader.startsWith('Bearer '))
        return res.status(401).json({ message: 'Unauthorized access.' });

    const token = authHeader.split(' ')[1];

    try {
        
        const decoded = jwt.verify(token, secret);
        req.userId = decoded.userId; // Attach user ID to the request object
        next();

    } catch (error) {

        return res.status(403).json({ message: 'Invalid or expired token.' });

    }
};

const loginUser = async (req, res) => {
    const { email, password } = req.body;

    if (!email || !password)
        return res.status(400).json({ message: 'Email and password are required.' });

    try {
        const user = await db.query('SELECT id, name, password_hash FROM users WHERE email = $1', [email]);

        if (user.rowCount === 0)
            return res.status(401).json({ statusCode: 401, message: 'Invalid email or password.' });

        const passwordMatch = await bcrypt.compare(password, user.rows[0].password_hash);

        if (!passwordMatch)
            return res.status(401).json({ statusCode: 401, message: 'Invalid email or password.' });

        const token = generateToken(user.rows[0].id); // Generate JWT token on successful login
        const userId = user.rows[0].id;
        const name = user.rows[0].name;
        console.log(userId)

        return res.status(200).json({ 
            statusCode: 200,
            message: "User logged successfully",
            userId: userId.toString(),
            email: email,
            name: name,
            token: token
        })

    } catch (error) {
        res.status(500).json({ message: 'Internal server error.' });

    }
}

const registerUser = async (req, res) => {

    const { name, email, password, identification } = req.body;

    if (!name || !email || !password || !identification)
        return res.status(400).json({ message: 'name, email, identification and password are required.' });


    const passwordHash = await bcrypt.hash(password, 10);

    // Validate for existing name or email (replace with database checks)
    try {
        
        const existingUser = await db.query('SELECT * FROM users WHERE email = $1', [email]);
        if(existingUser.rowCount !== 0)
            return res.status(401).json({ statusCode: 401, message: 'User already exists!' });

        const insertedUser = await db.query(`INSERT INTO users 
                        (name, email, password_hash, identification, user_type_id) 
                        VALUES ($1, $2, $3, $4, $5) RETURNING id`, [name, email, passwordHash, identification, 1]);
        
        const userId = insertedUser.rows[0].id;

        const token = generateToken(userId)

        return res.status(200).json({ 
            statusCode: 200,
            message: "User registered successfully",
            userId: userId.toString(),
            email: email,
            name: name,
            token: token
        })
        
    } catch (err) {

        res.status(500).json({ statusCode: 500, message: "Internal server error" });
        

    }

}

module.exports = {
    loginUser: loginUser,
    registerUser: registerUser,
    verifyToken: verifyToken,
    generateToken: generateToken
}