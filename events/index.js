const db = require('../db/db');

const getPublicEvents = async (req, res) => {
    try {
        const events = await db.query(`SELECT * FROM events`);
        return res.status(200).json(events.rows)
    } catch (error) {
        return res.status(500).json({ message: "Internal server error" })
    }
}

const getEventsGallery = async (req, res) => {
    const eventId = req.params.id
    console.log(eventId)
    try {
        const events = await db.query(`SELECT * FROM events_gallery WHERE event_id = $1 AND status_id = 1`, [eventId])
        return res.status(200).json(events.rows)
    } catch (error) {
        console.log(error)
        return res.status(500).json({ message: "Internal server error" })
    }
}

module.exports = {
    getPublicEvents: getPublicEvents,
    getEventsGallery: getEventsGallery
}