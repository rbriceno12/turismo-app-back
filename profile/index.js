const db = require('../db/db');


const getProfileData = async (req, res) => {
    
        const userId = req.userId
    
        try {
    
            const profileData = await db.query(
                `SELECT u.name, 
                        u.email, 
                        u.created_at, 
                        u.identification,
                        ut.name as user_type 
                        FROM users u
                        INNER JOIN user_types ut
                        ON u.user_type_id = ut.id 
                        WHERE u.id = $1`, [userId]
            )
    
            return res.status(200).json(profileData.rows[0])
    
        } catch (error) {
    
            return res.status(500).json({ message: "Internal server error" })
    
        }
}

module.exports = {
    getProfileData: getProfileData
}