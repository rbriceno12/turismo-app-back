const db = require('../db/db');


const getCategories = async (req, res) => {

    try {

        const categories = await db.query(`SELECT * FROM categories`);

        return res.status(200).json( categories.rows )

    } catch (error) {

        return res.status(500).json( {message: "Internal server error"} )

    }

}

module.exports = {
    getCategories:  getCategories
}