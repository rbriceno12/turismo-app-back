-- public.amenities definition

-- Drop table

-- DROP TABLE public.amenities;

CREATE TABLE public.amenities (
	id serial4 NOT NULL,
	"name" varchar(50) NOT NULL,
	description text NOT NULL,
	icon varchar NULL,
	CONSTRAINT amenities_name_key UNIQUE (name),
	CONSTRAINT amenities_pkey PRIMARY KEY (id)
);


-- public.establishment_categories definition

-- Drop table

-- DROP TABLE public.establishment_categories;

CREATE TABLE public.establishment_categories (
	id serial4 NOT NULL,
	"name" varchar(50) NOT NULL,
	description text NULL,
	icon varchar NULL,
	CONSTRAINT establishment_categories_name_key UNIQUE (name),
	CONSTRAINT establishment_categories_pkey PRIMARY KEY (id)
);


-- public.labels definition

-- Drop table

-- DROP TABLE public.labels;

CREATE TABLE public.labels (
	id serial4 NOT NULL,
	"name" varchar(50) NOT NULL,
	description text NULL,
	icon varchar NULL,
	CONSTRAINT labels_name_key UNIQUE (name),
	CONSTRAINT labels_pkey PRIMARY KEY (id)
);


-- public.status definition

-- Drop table

-- DROP TABLE public.status;

CREATE TABLE public.status (
	id serial4 NOT NULL,
	"name" varchar(50) NOT NULL,
	created_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	updated_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	CONSTRAINT status_name_key UNIQUE (name),
	CONSTRAINT status_pkey PRIMARY KEY (id)
);


-- public.user_types definition

-- Drop table

-- DROP TABLE public.user_types;

CREATE TABLE public.user_types (
	id serial4 NOT NULL,
	"name" varchar(50) NOT NULL,
	description text NULL,
	CONSTRAINT user_types_name_key UNIQUE (name),
	CONSTRAINT user_types_pkey PRIMARY KEY (id)
);


-- public.users definition

-- Drop table

-- DROP TABLE public.users;

CREATE TABLE public.users (
	id serial4 NOT NULL,
	"name" varchar(50) NOT NULL,
	email varchar(100) NOT NULL,
	password_hash bpchar(60) NOT NULL,
	user_type_id int4 NOT NULL,
	identification varchar(100) NOT NULL,
	created_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	updated_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	CONSTRAINT users_email_key UNIQUE (email),
	CONSTRAINT users_pkey PRIMARY KEY (id),
	CONSTRAINT users_user_type_id_fkey FOREIGN KEY (user_type_id) REFERENCES public.user_types(id)
);


-- public.establishments definition

-- Drop table

-- DROP TABLE public.establishments;

CREATE TABLE public.establishments (
	id serial4 NOT NULL,
	"name" varchar(150) NOT NULL,
	description text NOT NULL,
	lat varchar(50) NOT NULL,
	lng varchar(50) NOT NULL,
	category_id int4 NOT NULL,
	status_id int4 NOT NULL,
	user_id int4 NOT NULL,
	created_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	updated_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	CONSTRAINT establishments_pkey PRIMARY KEY (id),
	CONSTRAINT establishments_establishment_categories_id_fkey FOREIGN KEY (category_id) REFERENCES public.establishment_categories(id),
	CONSTRAINT establishments_status_id_fkey FOREIGN KEY (status_id) REFERENCES public.status(id),
	CONSTRAINT users_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id)
);


CREATE TABLE public.bookings (
	id serial4 NOT NULL,
	establishment_id int4 NOT NULL,
    user_id int 4 NOT NULL,
	description text NOT NULL,
	status_id int4 NOT NULL,
    date_in timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
    date_out timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	created_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	updated_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	CONSTRAINT bookings_pkey PRIMARY KEY (id),
	CONSTRAINT bookings_status_id_fkey FOREIGN KEY (status_id) REFERENCES public.status(id),
    CONSTRAINT establishment_id_fkey FOREIGN KEY (establishment_id) REFERENCES public.establishments(id),
	CONSTRAINT users_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id)
);


-- public.establishments_amenities definition

-- Drop table

-- DROP TABLE public.establishments_amenities;

CREATE TABLE public.establishments_amenities (
	id serial4 NOT NULL,
	establishment_id int4 NOT NULL,
	amenity_id int4 NOT NULL,
	status_id int4 NOT NULL,
	created_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	updated_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	CONSTRAINT establishments_amenities_pkey PRIMARY KEY (id),
	CONSTRAINT amenities_id_fkey FOREIGN KEY (amenity_id) REFERENCES public.amenities(id),
	CONSTRAINT establishments_amenities_status_id_fkey FOREIGN KEY (status_id) REFERENCES public.status(id),
	CONSTRAINT establishments_id_fkey FOREIGN KEY (establishment_id) REFERENCES public.establishments(id)
);


-- public.establishments_comments definition

-- Drop table

-- DROP TABLE public.establishments_comments;

CREATE TABLE public.establishments_comments (
	id serial4 NOT NULL,
	"comment" text NOT NULL,
	establishment_id int4 NOT NULL,
	status_id int4 NOT NULL,
	user_id int4 NOT NULL,
	created_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	updated_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	CONSTRAINT establishments_comments_pkey PRIMARY KEY (id),
	CONSTRAINT establishments_comments_status_id_fkey FOREIGN KEY (status_id) REFERENCES public.status(id),
	CONSTRAINT establishments_id_fkey FOREIGN KEY (establishment_id) REFERENCES public.establishments(id),
	CONSTRAINT users_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id)
);


-- public.establishments_gallery definition

-- Drop table

-- DROP TABLE public.establishments_gallery;

CREATE TABLE public.establishments_gallery (
	id serial4 NOT NULL,
	url varchar(150) NOT NULL,
	establishment_id int4 NOT NULL,
	status_id int4 NOT NULL,
	created_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	updated_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	CONSTRAINT establishments_gallery_pkey PRIMARY KEY (id),
	CONSTRAINT establishments_gallery_status_id_fkey FOREIGN KEY (status_id) REFERENCES public.status(id),
	CONSTRAINT establishments_id_fkey FOREIGN KEY (establishment_id) REFERENCES public.establishments(id)
);


-- public.establishments_labels definition

-- Drop table

-- DROP TABLE public.establishments_labels;

CREATE TABLE public.establishments_labels (
	id serial4 NOT NULL,
	establishment_id int4 NOT NULL,
	label_id int4 NOT NULL,
	status_id int4 NOT NULL,
	created_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	updated_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	CONSTRAINT establishments_labels_pkey PRIMARY KEY (id),
	CONSTRAINT establishments_id_fkey FOREIGN KEY (establishment_id) REFERENCES public.establishments(id),
	CONSTRAINT establishments_labels_status_id_fkey FOREIGN KEY (status_id) REFERENCES public.status(id),
	CONSTRAINT labels_id_fkey FOREIGN KEY (label_id) REFERENCES public.labels(id)
);


-- public.establishments_likes definition

-- Drop table

-- DROP TABLE public.establishments_likes;

CREATE TABLE public.establishments_likes (
	id serial4 NOT NULL,
	establishment_id int4 NOT NULL,
	status_id int4 NOT NULL,
	user_id int4 NOT NULL,
	created_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	updated_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	CONSTRAINT establishments_likes_pkey PRIMARY KEY (id),
	CONSTRAINT establishments_id_fkey FOREIGN KEY (establishment_id) REFERENCES public.establishments(id),
	CONSTRAINT establishments_likes_status_id_fkey FOREIGN KEY (status_id) REFERENCES public.status(id),
	CONSTRAINT users_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id)
);


-- public.establishments_rating definition

-- Drop table

-- DROP TABLE public.establishments_rating;

CREATE TABLE public.establishments_rating (
	id serial4 NOT NULL,
	rate int4 NOT NULL,
	establishment_id int4 NOT NULL,
	status_id int4 NOT NULL,
	user_id int4 NOT NULL,
	created_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	updated_at timestamptz DEFAULT CURRENT_TIMESTAMP NULL,
	CONSTRAINT establishments_rating_pkey PRIMARY KEY (id),
	CONSTRAINT establishments_id_fkey FOREIGN KEY (establishment_id) REFERENCES public.establishments(id),
	CONSTRAINT establishments_rating_status_id_fkey FOREIGN KEY (status_id) REFERENCES public.status(id),
	CONSTRAINT users_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id)
);