const express = require('express')
const cors = require('cors')

const app = express()

require('dotenv').config(); // Load environment variables from .env file
const port = process.env.PORT || 8080; // Use environment variable or default port

const bodyParser = require('body-parser');
// Middleware
app.use(bodyParser.json()); // Parse JSON data in request body
app.use(bodyParser.urlencoded({ extended: true })); // Parse form data
app.use(cors({
  origin: '*',
}))

app.use('/public/', express.static('public/'));


const auth           = require('./auth/index')
const establishments = require('./establishments/index')
const categories     = require('./categories/index')
const events         = require('./events/index')
const booking        = require('./booking/index')
const profile        = require('./profile/index')


//Authorization/Users
app.post('/register', (req, res) => {

  auth.registerUser(req, res)
  
});

app.post('/login', (req, res) => {

  auth.loginUser(req, res)

});


//Establishments

app.post('/establishments', auth.verifyToken, (req, res) => {
  establishments.createEstablishment(req, res)
})

app.post('/establishments/create-gallery', auth.verifyToken, (req, res) => {
  establishments.insertEstablishmentGallery(req, res)
})

app.post('/establishments/create-amenities', auth.verifyToken, (req, res) => {
  establishments.insertEstablishmentAmenities(req, res)
})

app.post('/establishments/create-labels', auth.verifyToken, (req, res) => {
  establishments.insertEstablishmentLabels(req, res)
})

app.put('/establishments/:id', auth.verifyToken, (req, res) => {
  establishments.updateEstablishment(req, res)
})

app.delete('/establishments/:id', auth.verifyToken, (req, res) => {
  establishments.deleteEstablishment(req, res)
})

app.get('/establishments', (req, res) => {
  establishments.getPublicEstablishments(req, res)
})

app.get('/establishments/search/', (req, res) => {
  establishments.searchEstablishments(req, res)
})

app.get('/establishments/gallery/:id', (req, res) => {
  establishments.getEstablishmentGallery(req, res)
})

//Categories
app.get('/categories', (req, res) => {
  categories.getCategories(req, res)
})

//Events
app.get('/events', (req, res) => {
  events.getPublicEvents(req, res)
})

app.get('/events/gallery/:id', (req, res) => {
  events.getEventsGallery(req, res)
})

//Booking
app.post('/booking', auth.verifyToken, (req, res) => {
  booking.createBooking(req, res)
})

app.get('/bookings', auth.verifyToken, (req, res) => {
  booking.getBookings(req, res)
})

//Profile
app.get('/profile', auth.verifyToken, (req, res) => {
  profile.getProfileData(req, res)
})

app.listen(port, () => {
  console.log(`App listening on port ${port}`)
})