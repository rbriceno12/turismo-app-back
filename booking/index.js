const db = require('../db/db');

const createBooking = async (req, res) => {
    const {
        establishment_id,
        user_id,
        booking_date,
        booking_time,
        booking_status
    } = req.body

    try {
        const booking = await db.query(`INSERT INTO bookings 
            (establishment_id, user_id, booking_date, booking_time, booking_status, status_id)
            VALUES
            ($1, $2, $3, $4, $5, 1)
            RETURNING *
        `, [establishment_id, user_id, booking_date, booking_time, booking_status])

        return res.status(200).json(booking.rows[0])

    } catch (error) {
        return res.status(500).json( {message: "Internal server error"} )
    }

}

const getBookings = async (req, res) => {

    try {
        const bookings = await db.query(
            `SELECT b.*, e.name, e.banner
            FROM bookings b
            INNER JOIN establishments e 
            ON b.establishment_id = e.id
            WHERE b.user_id = $1
            `, [req.userId]
        );
        return res.status(200).json(bookings.rows)
    } catch (error) {
        return res.status(500).json({ message: "Internal server error" })
    }
}

module.exports = {
    createBooking: createBooking,
    getBookings: getBookings
}