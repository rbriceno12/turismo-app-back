const db = require('../db/db')

const createEstablishment = async (req, res) => {

    const { name, 
            description, 
            lat, 
            lng, 
            category: categoryId, 
            user: userId
    } = req.body

    try {

        const establishment = await db.query(`INSERT INTO establishments 
            (name, description, lat, lng, category_id, user_id, status_id)
            VALUES
            ($1, $2, $3, $4, $5, $6, 1) RETURNING id
        `, [name, description, lat, lng, categoryId, userId])

        const establishmentId = establishment.rows[0].id;

        return res.status(200).json( {
            message: 'Establishment inserted successfully',
            id: establishmentId
        } )


    } catch (error) {

        return res.status(500).json( {message: "Internal server error"} )

    }

}

const insertEstablishmentGallery = (req, res) => {

    const {

        gallery: galleryObject,
        id: establishmentId 

    } = req.body

    try {

        Object.keys(galleryObject).forEach(key => {
            const value = galleryObject[key].file;
            db.query(`INSERT INTO establishments_gallery 
                (url, establishment_id, status_id)
                VALUES
                ($1, $2, 1)
            `, [value, establishmentId])
        });

        return res.status(200).json( {
            message: 'Establishment gallery inserted successfully',
            id: establishmentId
        })

    } catch (error) {
        return res.status(500).json( {message: "Internal server error"} )
    }

    

}

const insertEstablishmentAmenities = (req, res) => {
    const {

        amenities: amenitiesObject,
        id: establishmentId 

    } = req.body

    try {

        Object.keys(amenitiesObject).forEach(key => {
            const value = amenitiesObject[key].id;
            db.query(`INSERT INTO establishments_amenities 
                (amenity_id, establishment_id, status_id)
                VALUES
                ($1, $2, 1)
            `, [value, establishmentId])
        });

        return res.status(200).json( {
            message: 'Establishment amenity inserted successfully',
            id: establishmentId
        })

    } catch (error) {
        return res.status(500).json( {message: "Internal server error"} )
    }

    

}

const insertEstablishmentlabels = (req, res) => {
    const {

        labels: labelsObject,
        id: establishmentId 

    } = req.body

    try {

        Object.keys(labelsObject).forEach(key => {
            const value = labelsObject[key].id;
            db.query(`INSERT INTO establishments_labels 
                (amenity_id, establishment_id, status_id)
                VALUES
                ($1, $2, 1)
            `, [value, establishmentId])
        });

        return res.status(200).json( {
            message: 'Establishment label inserted successfully',
            id: establishmentId
        })

    } catch (error) {
        return res.status(500).json( {message: "Internal server error"} )
    }

}


const getPublicEstablishments = async (req, res) => {
    
    const { name, lat, lng, category} = req.query

    let page = 1

    req.query.page ? page = req.query.page : page = 1

    const itemsPerPage = 10

    const nameString = name ? ` AND name ILIKE '%${name}%'` : ''

    const categoryString = category ? ` AND category_id = ${category}` : ''
    
    try {
        
        const establishments = await db.query(
            `SELECT * 
            FROM establishments 
            WHERE status_id = 1 ${nameString}${categoryString} 
            LIMIT ${itemsPerPage} 
            OFFSET (${page} - 1) * ${itemsPerPage}`
        )

        
        return res.status(200).json({
            statusCode: 200,
            message: "Establishments retreived successfully",
            data: establishments.rows
        })

    } catch (error) {
        console.log(error)
        return res.status(500).json( {statusCode: 500, message: "Internal server error"} )

    }

}

const searchEstablishments = async (req, res) => {
    
    const { q = null, category = null, all = null } = req.query

    let query = ''

    if (q === null && category === null && all === null) {
        // Handle case when all query parameters are null
        query = `select 
                e.* 
                FROM establishments e
                INNER JOIN cities c
                ON e.city_id = c.id 
                INNER JOIN categories ca
                ON e.category_id = ca.id
                LIMIT 10
                `

    } else if (category && !q) {
        // Handle case when category is not null and q is null
        query = `select 
                e.* 
                FROM establishments e
                INNER JOIN categories ca
                ON e.category_id = ca.id
                WHERE ca.name ILIKE '%${category}%'`

    } else if (!category && q) {
        // Handle case when category is null and q is not null
        query = `select 
                e.* 
                FROM establishments e
                INNER JOIN cities c
                ON e.city_id = c.id 
                INNER JOIN categories ca
                ON e.category_id = ca.id
                WHERE c.name ILIKE '%${q}%'
                OR ca.name ILIKE '%${q}%'
                OR e.name ILIKE '%${q}%'`

    } else if (all === 'true') {
        // Handle case when all is true
        query = `select 
                e.* 
                FROM establishments e
                INNER JOIN cities c
                ON e.city_id = c.id 
                INNER JOIN categories ca
                ON e.category_id = ca.id
                LIMIT 10
                `

    }
    

    try {
        
        const establishments = await db.query(query)

        console.log({
            statusCode: 200,
            message: "Establishments retreived successfully",
            data: establishments.rows
        })

        return res.status(200).json({
            statusCode: 200,
            message: "Establishments retreived successfully",
            data: establishments.rows
        })

        

    } catch (error) {

        return res.status(500).json( {statusCode: 500, message: "Internal server error"} )

    }

}

const getPrivateEstablishments = async (req, res) => {
    
    const { name, lat, lng, category, page } = req.query

    const itemsPerPage = 5

    const nameString = name ? `AND name ILIKE '%${name}%'` : ''

    const latString = lat ? `AND lat = ${lat}` : ''
    
    const lngString = lng ? `AND lng = ${lng}` : ''

    const categoryString = category ? `AND category_id = ${category}` : ''

    try {
        
        const establishments = await db.query(
            `SELECT id, name, description, lat, lng 
            FROM establishments 
            WHERE status_id = 1 ${nameString} ${latString} ${lngString} ${categoryString} 
            LIMIT ${itemsPerPage} 
            OFFSET (${page} - 1) * ${itemsPerPage}`
        )
        
        return res.status(200).json({
            message: "Establishments retreived successfully",
            data: establishments.rows
        })

    } catch (error) {

        return res.status(500).json( {message: "Internal server error"} )

    }

}


const updateEstablishment = async (req, res) => {

    const establishmentId = req.params.id

    const { name, lat, lng, category, description } = req.body
    
    try {
        
        await db.query(`UPDATE establishments SET name = $1, description = $2, lat = $3, lng = $4, category_id = $5 WHERE id = $6`, [name, description, lat, lng, category, establishmentId])
        
        return res.status(200).json({
            message: "Establishment updated successfully"
        })

    } catch (error) {
        
        return res.status(500).json( {message: "Internal server error"} )

    }

}

const deleteEstablishment = async (req, res) => {

    const establishmentId = req.params.id

    try {
        
        await db.query(`UPDATE establishments SET status_id = 2 WHERE id = $1`, [establishmentId])
        
        return res.status(200).json({
            message: "Establishment deleted successfully"
        })

    } catch (error) {
        
        return res.status(500).json( {message: "Internal server error"} )

    }

}

const getEstablishmentGallery = async (req, res) => {
    const establishmentId = req.params.id
    try {
        const gallery = await db.query(`SELECT * FROM establishments_gallery WHERE establishment_id = $1 AND status_id = 1`, [establishmentId])
        return res.status(200).json(gallery.rows)
    } catch (error) {
        return res.status(500).json( {message: "Internal server error"} )
    }
}

module.exports = {
    createEstablishment: createEstablishment,
    insertEstablishmentAmenities: insertEstablishmentAmenities,
    insertEstablishmentGallery: insertEstablishmentGallery,
    insertEstablishmentlabels: insertEstablishmentlabels,
    getPublicEstablishments: getPublicEstablishments,
    getPrivateEstablishments: getPrivateEstablishments,
    updateEstablishment: updateEstablishment,
    deleteEstablishment: deleteEstablishment,
    searchEstablishments: searchEstablishments,
    getEstablishmentGallery: getEstablishmentGallery
}